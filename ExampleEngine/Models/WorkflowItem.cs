﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleEngine.Models
{
    public class WorkflowItem
    {
        public string type;
        public string id;
        public string name;
        public string description;
        public List<Pointer> pointers;
        public List<Parameter> parameters;
        public int status;

        public class Pointer
        {
            public string pointsTo;
            public string expression;
        }

        public class Parameter
        {
            public string key;
            public string type;
            public string value;
        }
    }
}
