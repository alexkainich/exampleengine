﻿using System;
using System.Collections.Generic;
using ExampleEngine.Engines;
using ExampleEngine.Models;
using ExampleEngine.Utils;
using ExampleEngine.Validations;

namespace ExampleEngine
{
    class Program
    {
        const string inputFile = "input.json";

        static void Main(string[] args)
        {
            JsonParser jsonParser = new JsonParser(inputFile);
            List<WorkflowItem> workflow = jsonParser.GetWorkflow();

            if (WorkflowValidations.Validate(workflow))
            {
                WorkflowEngine workflowEngine = new WorkflowEngine(workflow);
                workflowEngine.ProcessWorkflow();
            }

            Console.ReadLine();
        }
    }
}
