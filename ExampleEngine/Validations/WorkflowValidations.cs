﻿using ExampleEngine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleEngine.Validations
{
    public static class WorkflowValidations
    {
        private static List<WorkflowItem> workflow;

        public static bool Validate(List<WorkflowItem> wf)
        {
            workflow = wf;

            if (HasOneStartPoint() && HasOneEndPoint())
            {
                return true;
            }

            return false;
        }

        private static bool HasOneStartPoint()
        {
            var startingPoints = workflow.Where(w => w.type == "Start");

            if (!startingPoints.Any())
            {
                Console.WriteLine("Error. No \"Start\" found");
                return false;
            }
            else if (startingPoints.Count() > 1)
            {
                Console.WriteLine("Error. More than one \"Start\" found");
                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool HasOneEndPoint()
        {
            var endPoints = workflow.Where(w => w.type == "End");

            if (!endPoints.Any())
            {
                Console.WriteLine("Error. No \"End\" found");
                return false;
            }
            else if (endPoints.Count() > 1)
            {
                Console.WriteLine("Error. More than one \"End\" found");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
