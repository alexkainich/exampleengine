﻿using ExampleEngine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleEngine.Engines
{
    public class WorkflowEngine
    {
        private List<WorkflowItem> workflow;
        private Thread mainThread;
        private Random random = new Random();
        private int testvalue = 1;

        public WorkflowEngine(List<WorkflowItem> workflow)
        {
            this.workflow = workflow;
        }

        public void ProcessWorkflow()
        {
            WorkflowItem start = workflow.Where(w => w.type == "Start").Single();

            mainThread = Thread.CurrentThread;

            ProcessWorkflowItem(start);
        }

        private WorkflowItem ProcessWorkflowItem(object wfi)
        {
            WorkflowItem workflowItem = wfi as WorkflowItem;

            switch (workflowItem.type)
            {
                case "Parallel":
                    return ProcessParallel(workflowItem);
                case "Exclusive":
                    return ProcessExclusive(workflowItem, string.Concat("testvalue==", testvalue));
                default:
                    return ProcessActivity(workflowItem);
            }
        }


        private WorkflowItem ProcessParallel(WorkflowItem wfi)
        {
            if (wfi.pointers.Count == 1)
            {
                return wfi;
            }

            DoTask(wfi);

            var pointers = GetPointers(wfi);

            WorkflowItem syncGate = StartAndWaitTasks(pointers);

            if (syncGate != null)
            {
                DoTask(syncGate);

                return ProcessFirstPointer(syncGate);
            }

            return null;
        }

        private WorkflowItem ProcessExclusive(WorkflowItem wfi, string ifValue)
        {
            DoTask(wfi);

            string decisionNodeId = wfi.pointers.Where(p => p.expression == ifValue).Single().pointsTo;
            WorkflowItem decisionNode = workflow.Where(w => w.id == decisionNodeId).Single();

            return ProcessWorkflowItem(decisionNode);
        }

        private WorkflowItem ProcessActivity(WorkflowItem wfi)
        {
            DoTask(wfi);

            return ProcessFirstPointer(wfi);
        }


        private WorkflowItem ProcessFirstPointer(WorkflowItem wfi)
        {
            var pointers = GetPointers(wfi);

            if (pointers.Any())
                return ProcessWorkflowItem(pointers.First());
            else
                return null;
        }

        private WorkflowItem StartAndWaitTasks(List<WorkflowItem> wfitems)
        {
            List<WorkflowItem> syncGates = new List<WorkflowItem>();
            List<Task> tasks = new List<Task>();

            foreach (WorkflowItem item in wfitems)
            {
                Task task = Task.Factory.StartNew(() => { syncGates.Add(ProcessWorkflowItem(item)); });
                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

            if (syncGates.All(g => g == null))
                return null;
            else
                return syncGates.Where(g => g != null).First();
        }

        private void DoTask(WorkflowItem swf)
        {
            try
            {
                switch (swf.type)
                {
                    case "Activity":
                        Thread.Sleep(random.Next(0, 5) * 1000);
                        break;
                    default:
                        break;
                }

                Console.WriteLine(string.Concat(swf.type, " is \"Id\": ", swf.id));
            }
            catch (Exception ex)
            {
                // continue but change the testvalue
                testvalue = 0;
            }
        }

        private List<WorkflowItem> GetPointers(WorkflowItem wfi)
        {
            List<WorkflowItem> result = new List<WorkflowItem>();

            foreach (WorkflowItem.Pointer pointer in wfi.pointers)
            {
                result.Add(workflow.Where(t => t.id == pointer.pointsTo).Single());
            }

            return result;
        }
    }
}
