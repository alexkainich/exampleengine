﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using ExampleEngine.Models;

namespace ExampleEngine.Utils
{
    public class JsonParser
    {
        public dynamic jsonFile;

        public JsonParser(string file)
        {
            jsonFile = LoadJson(file);
        }

        public object GetNodeData()
        {
            throw new NotImplementedException();
        }

        public object GetLinkData()
        {
            throw new NotImplementedException();
        }

        public List<WorkflowItem> GetWorkflow()
        {
            JArray workflows = jsonFile["Content"][0]["Workflow"];

            return workflows.ToObject<List<WorkflowItem>>();
        }

        private dynamic LoadJson(string file)
        {
            using (StreamReader r = new StreamReader(file))
            {
                string json = r.ReadToEnd();

                return JObject.Parse(json);
            }
        }
    }
}
