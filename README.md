﻿## C# engine that can process a json file detailing a workflow design. The workflow can consist of parallel and synchronous processes

<img src="example.jpg">

### How to run:
- Download the source code and open with visual studio.

- The code is using the NuGet package "Newtonsoft.Json", which needs to be installed.


### General Comments:
- See the attached pdf within the project folder for more details.

- The code has been written following clean code practices.

- The engine processes input.json that contains a simple workflow of 2 parallel processes and a third process that starts once the first two finish.

- complexInput.json is an alternative JSON input file, which includes a more complicated workflow example.


### Implementation details:
- The code starts and reads the JSON file. From that, we process the "Workflow" object.

- Have included a sample validations file, which currently checks whether there is a single start and a single end in the workflow.
  However, more validations should ideally be added.

- The program starts in the "Main" function of the "Program.cs" file.

- The engine that handles the workflow is the "WorkflowEngine" class. 
  It is processed by starting from the "Start" node and by using the pointers to navigate from node to node.
  
  The parallel Gate is processed in the following way:
   - We have assumed that a parallel gate with only one pointer is a "closing" parallel gate. Whereas a gate with more than 1 pointers is a "starting" parallel gate.
   - So when the engine sees a starting parallel gate, it fires all the tasks in parallel threads and waits for them to complete before continuing.
   - The parallel threads return either null, or a closing parallel gate, to the thread that fired them.
   - We assume that all parallel threads (started by the same starting parallel gate) return the same closing parallel gate.
   - So if a closing parallel gate is returned, then the thread continues with the next node.
 
- An activity simply prints its id in the console and takes from 0 to 5 seconds to complete, randomly.


### TODO:
- More validations.

- Implement an abort method that will abort all active threads.

- Relax assumptions in parallel gate processing.
